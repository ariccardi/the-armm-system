# The ARMM System

## Introduction
The ARMM System [1] is a mobile electromagnetic coil mounted on a 6DOFs serial manipulator.

## References
[1] J. Sikorski, C. M. Heunis, F. Franco, and S. Misra, “The ARMM System: An Optimized Mobile Electromagnetic Coil for Non-Linear Actuation of Flexible Surgical Instruments,” in IEEE Transactions on Magnetics, vol. 55, pp. 1-9, 2019.<br />
